package zone.haze.alevel;

public class Obscure {
    private static volatile Obscure currentInstance = new Obscure();

    public static synchronized Obscure instance() {
        return currentInstance;
    }

    private Obscure() {}

}
